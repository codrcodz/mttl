import React from 'react';
import data from '../data/data.json';
import * as RJD from '../Components/diagram/main';

let discSizes = {};
let nodes = [];
let links = [];
let ports = [];
const phaseColors = ['rgb(0, 192, 255)','rgb(89, 179, 0)','rgb(242, 135, 13)','rgb(255, 0, 0)']


Object.keys(data).forEach(discipline => {
  data[discipline].phases.forEach(phase =>{
    if(discipline in discSizes){
      discSizes[discipline] = Math.max(discSizes[discipline], phase.length)
    }
    else{
      discSizes[discipline] = phase.length;
    }
    phase.forEach(role =>{
      if(role.linksTo.length > 0){
      role.linksTo.forEach(element => {
        links.push({from: (role.id + "-out"), to: (element + "-in")});
      })};
    });
  });
});


export class RoadMap extends React.Component {
  constructor(props) {
    super(props);
    this.xSize = 200;
    this.ySize = 100;
    this.Yspacing = 40;
    this.Xspacing = 70;
    this.horizontalOffset = 60;
    this.verticalOffset = 40;
    this.state = { viewing: "Nothing" };
    this.updateView = this.updateView.bind(this);
    // Setup the diagram engine
    this.engine = new RJD.DiagramEngine();
    this.engine.registerNodeFactory(new RJD.DefaultNodeFactory());
    this.engine.registerLinkFactory(new RJD.DefaultLinkFactory());
    // Setup the diagram model
    this.model = new RJD.DiagramModel();
  }

  updateView(viewText) {
    this.setState({ viewing: viewText });
  }

  componentDidMount() {
    setTimeout(() => {
      this.testSerialization();
    }, 1000);
  }

  createNode(options) {
    const { name, color, phase, nodePos, training, viewer, x, y } = options;
    var node = new RJD.DefaultNodeModel(name, color, phase, nodePos, training, viewer);
    node.x = x;
    node.y = y;
    node.phase = phase;
    node.nodePos = nodePos;
    node.itemList = training;
    node.viewer = viewer;
    return node;
  }

  createPort(node, options) {
    const { isInput, id, name } = options;
    return node.addPort(new RJD.DefaultPortModel(isInput, id, name));
  }

  linkNodes(port1, port2) {
    const link = new RJD.LinkModel();
    link.setSourcePort(port1);
    link.setTargetPort(port2);
    return link;
  }

  testSerialization() {
    const { engine, model } = this;
    // We need this to help the system know what models to create form the JSON
    engine.registerInstanceFactory(new RJD.DefaultNodeInstanceFactory());
    engine.registerInstanceFactory(new RJD.DefaultPortInstanceFactory());
    engine.registerInstanceFactory(new RJD.LinkInstanceFactory());
    // Serialize the model
    const str = JSON.stringify(model.serializeDiagram());
    // Deserialize the model
    const model2 = new RJD.DiagramModel();
    model2.deSerializeDiagram(JSON.parse(str), engine);
    engine.setDiagramModel(model2);
  }

  makeNode(role, phaseCounter, nodeCounter, discSpacing, discMax, align) {
    //how many alignment correct segments to pad to the node
    let alignment = (align * (nodeCounter + 1) - (this.ySize / 2)) / 2;
    const node1 = this.createNode({
      name: role.id,
      color: phaseCounter < phaseColors.length ? phaseColors[phaseCounter] : phaseColors[0],
      phase: phaseCounter + 1,
      nodePos: nodes.length + 1,
      training: role.courses,
      viewer: this.updateView,
      x: this.xSize * phaseCounter + this.Xspacing * phaseCounter + this.horizontalOffset,
      y: this.ySize * nodeCounter + this.Yspacing * nodeCounter + this.verticalOffset + alignment + discSpacing,
    });
    const port1in = this.createPort(node1, {
      isInput: true,
      id: role.id + "-in",
    });
    const port1out = this.createPort(node1, {
      isInput: false,
      id: role.id + "-out",
    });
    nodes.push(node1);
    ports[node1.name + "-in"] = port1in;
    ports[node1.name + "-out"] = port1out;
    return null;
  }

  render() {
    const { engine, model } = this;
    let discSpacing = 0;
    Object.keys(data).forEach(discipline => {
      let discMax = discSizes[discipline] * this.ySize + discSizes[discipline] * this.Yspacing;
      data[discipline].phases.forEach((phase, i) => {
        phase.forEach((role, j) => {
          let align = 0;
          if (phase.length < discSizes[discipline]) {
            align = (discMax - phase.length * (this.ySize + this.Yspacing)) / phase.length;
          }
          this.makeNode(role, i, j, discSpacing, discMax, align);
        });
      });
      discSpacing += discMax;
    });
    nodes.forEach(element => {
      model.addNode(element);
    });
    links.forEach(element => {
      model.addLink(this.linkNodes(ports[element.from], ports[element.to]));
    });
    // Load the model into the diagram engine
    engine.setDiagramModel(model);
    // Render the canvas
    return <RJD.DiagramWidget diagramEngine={engine} actions={{zoom: false, deleteItems: false, moveItems: false}}/>;
  }
}
