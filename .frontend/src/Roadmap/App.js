import React from 'react';
import { RoadMap } from './RoadMap';

function App() {
  // This controlls the direction components are displayed in one column or row, depending on the configuration
  const div_outer = {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    height: "150vh",
    margin: "auto"
  };
  return (
    <div style={div_outer}>
      <RoadMap/>
    </div>
  );
}

export default App;
