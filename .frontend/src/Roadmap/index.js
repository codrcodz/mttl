import React, {Component} from 'react'
import App from './App'
import './index.scss'
import '../Components/diagram/sass.scss'

class Roadmap extends Component {
    render() {
        return (
            <div>
                <h1 style={{paddingBottom: "15px"}}>RoadMap</h1>
                <App/>
            </div>
        );
    }
}

export default Roadmap;