import React, {Component} from 'react'
import {Route, Link, BrowserRouter} from "react-router-dom";
import Table from '../Components/Table'
import {YELLOW, BLUE, GREEN} from '../helpers'
import './Home.css'
import DropDown from '../Components/DropDown'

const curr_path = window.location.pathname;

let useful_links = [
    {"name": "Training Lessons", "url": "https://gitlab.com/90cos/public/training"},
    {"name": "Preperation", "url": "https://gitlab.com/90cos/public/evaluations/preparatory"},
    {"name": "Wiki", "url": "https://gitlab.com/90cos/mttl/-/wikis/home"}
]

let wr_spec_columns = [
    {field: 'wr_spec', title: 'Work Role/Specialization', sortable: true},
    {field: 'Tasks', title: 'Tasks'},
    {field: 'Abilities', title: 'Abilities'},
    {field: 'Skills', title: 'Skills'},
    {field: 'Knowledge', title: 'Knowledge'},
];
let nowr_spec_columns = [
    {field: 'Tasks', title: 'Tasks'},
    {field: 'Abilities', title: 'Abilities'},
    {field: 'Skills', title: 'Skills'},
    {field: 'Knowledge', title: 'Knowledge'},
];
let ttl_columns = [
    {field: '_id', title: 'ID', sortable: true},
    {field: 'parent', title: 'Parent(s)', class: "children_description", sortable: true},
    {field: 'description', title: 'Description', class: "header_description", sortable: true},
    {field: 'children', title: 'Children', class: "children_description", sortable: true},
    {field: 'topic', title: 'Topic', sortable: true},
    {field: 'proficiency', title: 'Proficiency', sortable: true},
    {field: 'training', title: 'Training Covered', class: "extra_header_description", sortable: true},
    {field: 'eval', title: 'Eval Covered', class: "extra_header_description", sortable: true},
    {field: 'comments', title: 'Comments', class: "extra_header_description", sortable: true}
]

class Home extends Component {
    get_all_table_routes(data) {
        let nav_list = [];
        let route_list = [];

        for(let [key, val] of Object.entries(data)) {
            nav_list.push(<li key={key.toLowerCase()}><Link to={`${curr_path + key.toLowerCase()}`}>{key}</Link></li>)
            route_list.push(<Route 
                key={key.toLowerCase()}
                path={`${curr_path + key.toLowerCase()}`} 
                render={props => <Table 
                    {...props}
                    name={key.toLowerCase()} 
                    title={key.replace(/-/g, ' ')}
                    data={val} 
                    columns={ttl_columns}
                    metrics={this.props.metrics_data['work-roles'][key] || this.props.metrics_data['specializations'][key]}
                    rowStyle={true}
                />}
            />)
        }

        return [nav_list, route_list];
    }

    render() {
        const links_list = this.get_all_table_routes(this.props.ttl_data)

        return (
            <BrowserRouter>
            <div>
                <DropDown name={"Useful Links"} dropStyle={{"margin-bottom": "-52px", "margin-right": "130px", "float": "right"}}>
                    {useful_links.map(link => (
                        <li><a href={link.url} target="_blank" rel="noopener noreferrer">{link.name}</a></li>
                    ))}
                </DropDown>
                <DropDown name={"Table Select"} dropStyle={{"margin-bottom": "-52px", "float": "right"}}>
                    <li><Link to={`${curr_path}`}>MTTL</Link></li>
                    {links_list[0]}
                </DropDown>
                <div className="legend_column">
                    <div className="legend_group">
                        <div style={{backgroundColor: BLUE}} className="legend_icon"></div>
                        <p>Training Only</p>
                    </div>
                    <div className="legend_group">
                        <div style={{backgroundColor: YELLOW}} className="legend_icon"></div>
                        <p>Evaluation Only</p>
                    </div>
                    <div className="legend_group">
                        <div  style={{backgroundColor: GREEN}} className="legend_icon"></div>
                        <p>Evaluation and Training</p>
                    </div>
                </div>
                <div className="tableContent">
                    <Route 
                        exact path={`${curr_path}`}
                        render={props => <Table 
                            {...props}
                            name="mttl" 
                            title="MTTL" 
                            data={this.props.mttl_data} 
                            metrics={this.props.metrics_data['MTTL']}
                            rowStyle={true} 
                        />}
                    />
                    {links_list[1]}                  
                </div>                
                <Table name="wrspec" title="Work Roles/Specializations" data={this.props.wr_spec} columns={wr_spec_columns} />
                <Table name="nowrspec" title="KSATs without Work Roles/Specializations" data={this.props.nowr_spec} columns={nowr_spec_columns} metrics={this.props.metrics_data['nowrspec']} />
            </div>
            </BrowserRouter>
        );
    }
}

export default Home;