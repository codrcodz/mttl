import React, {Component} from 'react';
import {Route, NavLink, HashRouter} from "react-router-dom";
import logo from './logo.svg';
import forkMe from './fmog.png'
import './App.css';


import Home from "./Home";
import Metrics from "./Metrics";
import Roadmap from "./Roadmap";

import * as mttl_data from './data/MTTL.min.json';
import * as ttl_data from './data/TTLs.min.json';
import * as nowr_spec from './data/nowr_spec.min.json';
import * as wr_spec from './data/wr_spec.min.json';
import * as metrics_data from './data/metrics.min.json';

const repo = "https://gitlab.com/90cos/mttl"

class App extends Component {
  renderMergedProps = (component, ...rest) => {
    const finalProps = Object.assign({}, ...rest);
    return (
      React.createElement(component, finalProps)
    );
  }

  render() {
    return (
      <HashRouter>
        <div>
          <div className="headerBar">
            <a className="forkMe" href={repo}>
                <img src={forkMe} alt={forkMe} />
            </a>
            <img src={logo} className="App-logo" alt="logo" />
            <NavLink className="site-title" to="/">90 COS Master Training Task List</NavLink>
            <div>
              <ul className="header">
                <li><NavLink to="/metrics">Metrics</NavLink></li>
                <li><NavLink to="/roadmap">Roadmap</NavLink></li>
              </ul>
            </div>
          </div>
          <div className="content">
            <Route 
              exact path="/" 
              render={props => <Home {...props} 
                mttl_data={mttl_data.default}
                ttl_data={ttl_data.default}
                metrics_data={metrics_data.default}
                nowr_spec={nowr_spec.default}
                wr_spec={wr_spec.default}
              />}
            />
            <Route 
              path="/metrics" 
              render={props => <Metrics {...props}
                metrics_data={metrics_data.default}
              />}
            />
            <Route path="/Roadmap" component={Roadmap}/>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default App;
