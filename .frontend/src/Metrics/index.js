import React, {Component} from 'react';
import './Metrics.css';

import MetricsSection from '../Components/MetricsSection';

class Metrics extends Component {
    getMetricsSections = (data) => {
        let section_list = [];
        for(let [key, val] of Object.entries(data)) {
            section_list.push(<MetricsSection name={key} title={key.replace(/-/g, ' ')} data={val} key={key} />)
        }
        return section_list;
    }

    render() {
        const data = this.props.metrics_data;
        let wr_section_list = this.getMetricsSections(data['work-roles']);
        let spec_section_list = this.getMetricsSections(data['specializations']);

        return (
            <div>
                <div className="metrics">
                    <h1>Metrics</h1>
                    <MetricsSection name="mttl" title="MTTL Coverage" data={data.MTTL} />
                </div>
                <div className="metrics">
                    <h1>Work Roles</h1>
                    {wr_section_list}
                </div>
                <div className="metrics">
                    <h1>Specializations</h1>
                    {spec_section_list}
                </div>
            </div>
        );
    }
}

export default Metrics;