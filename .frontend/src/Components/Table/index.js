import React, {Component} from 'react';
import { rowStyle } from '../../helpers';
import './Table.css'

import MetricsBar from '../MetricsBar'

class Table extends Component {
    constructor(props) {
        super(props);
        this.searchName = this.props.name + 'table';
    }

    componentDidMount() {
        const name = this.props.name;
        let columns = this.props.columns;
        let defaultSettings= {
            data: this.props.data,
            height: 550,
            search: true,
            pagination: true,
            pageSize: 20,
            showFullscreen: true,
            showColumns: true,
            showExport: true,
            exportDataType: 'all',
            exportTypes: ['csv', 'excel'],
            exportOptions: {
                    ignoreColumn: [],
                    fileName: function() {
                        return name + '-export'
                    }
            },
            columns: columns
        };

        if (this.props.rowStyle) {
            defaultSettings.rowStyle = rowStyle;
        }

        if (!columns) {
            defaultSettings.columns = [
                {field: '_id', title: 'ID', sortable: true},
                {field: 'requirement_src', title: 'Source', sortable: true},
                {field: 'requirement_owner', title: 'Owner', sortable: true},
                {field: 'parent', title: 'Parent(s)', class: "children_description", sortable: true},
                {field: 'description', title: 'Description', class: "header_description", sortable: true},
                {field: 'children', title: 'Children', class: "children_description", sortable: true},
                {field: 'work-roles/specializations', title: 'Work Roles/Specializations', sortable: true},
                {field: 'topic', title: 'Topic', sortable: true},
                {field: 'training', title: 'Training Covered', class: "extra_header_description", sortable: true},
                {field: 'eval', title: 'Eval Covered', class: "extra_header_description", sortable: true},
                {field: 'comments', title: 'Comments', class: "extra_header_description", sortable: true}
            ]
        }

        /*global $*/
        $('.' + this.searchName).bootstrapTable(defaultSettings)
        if (defaultSettings.columns[defaultSettings.columns.length - 1].field === 'comments') {
            $('.' + this.searchName).bootstrapTable('hideColumn', 'comments');
        }
    }
    render() {
        const classN = this.searchName + ' table table-sm table-dark table-striped table-bordered'
        
        let titleHeader = <h1 style={{ marginBottom: "-50px" }}>{this.props.title}</h1>;
        let metricsBarTag = <div></div>;

        if (this.props.metrics) {
            titleHeader = <h1>{this.props.title}</h1>;
            metricsBarTag = <MetricsBar id={this.props.name + "_wte"} percent={this.props.metrics['percent-total']} />;
        }

        return (
            <div className="table_container">
                {titleHeader}
                {metricsBarTag}
                <table className={classN}></table>
            </div>
        );
    }
}

export default Table;