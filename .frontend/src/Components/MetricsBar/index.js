import React, {Component} from 'react'
import { setMetrix } from '../../helpers'
import './MetricsBar.css'

class MetricsBar extends Component {
    async componentDidMount() {
        await setMetrix(this.props.id, this.props.percent);
    }
    render() {
        return (
            <div id="metrics_bar">
                <div id={this.props.id}>1%</div>
            </div>
        );
    }
}

export default MetricsBar;