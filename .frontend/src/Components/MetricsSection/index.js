import React, {Component} from 'react'
import './MetricsSection.css'

import MetricsBar from '../MetricsBar'

class MetricsSection extends Component {
    render() {        
        return (
            <div className="coverage_container">
                <h3>{this.props.title}</h3>
                <div className="item_metrics_container">
                    <div className="metrics_container">
                        <h6>Training and Eval</h6>
                        <MetricsBar id={this.props.name + "_wte"} percent={this.props.data['percent-total']} />
                    </div>
                    <div className="metrics_container">
                        <h6>Training</h6>
                        <MetricsBar id={this.props.name + "_wt"} percent={this.props.data['percent-training']} />
                    </div>
                    <div className="metrics_container">
                        <h6>Eval</h6>
                        <MetricsBar id={this.props.name + "_we"} percent={this.props.data['percent-eval']} />
                    </div>
                </div>
            </div>
        );
    }
}

export default MetricsSection;