## Pipeline Overview

| Training/Test Bank | Lesson | Pipeline Status | Study Repo Status |
| ---           | ------ | --------------- | ----------------- |
| Training | Python | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/python/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/python/-/commits/master) | N/A |
| Training | C-Programming | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/C-Programming/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/C-Programming/-/commits/master) | N/A |
| Training | Network-Programming | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/network-programming/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/network-programming/-/commits/master) | N/A |
| Training | Introduction to Git | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/introduction-to-git/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/introduction-to-git/-/commits/master) | N/A |
| Training | Cpp-Programming | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/cpp-programming/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/cpp-programming/-/commits/master) | N/A |
| Training | Pseudocode | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/pseudocode/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/pseudocode/-/commits/master) | N/A |
| Training | Debugging | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/debugging/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/debugging/-/commits/master) | N/A |
| Training | Powershell | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/powershell/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/powershell/-/commits/master) | N/A |
| Training | Algorithms | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/algorithms/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/algorithms/-/commits/master) | N/A |
| Training | Reverse-Engineering | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/reverse-engineering/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/reverse-engineering/-/commits/master) | N/A |
| Training | Assembly | [![pipeline status](https://gitlab.com/90cos/public/training/lessons/assembly/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/lessons/assembly/-/commits/master) | N/A |
| Test Bank | Basic-Dev | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/basic-dev/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/basic-dev/-/commits/master) | [![pipeline status](https://gitlab.com/90cos/public/evaluations/preparatory/basic-dev/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/evaluations/preparatory/basic-dev/-/commits/master) |
| Test Bank | Basic-PO | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/basic-po/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/basic-po/-/commits/master) | [![pipeline status](https://gitlab.com/90cos/public/evaluations/preparatory/basic-po/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/evaluations/preparatory/basic-po/-/commits/master) |
| Test Bank | Senior-Dev-Linux | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/senior-dev-linux/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/senior-dev-linux/-/commits/master) | [![pipeline status](https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-linux/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-linux/-/commits/master) |
| Test Bank | Senior-Dev-Windows | [![pipeline status](https://gitlab.com/90cos/Private-test-bank/senior-dev-windows/badges/master/pipeline.svg)](https://gitlab.com/90cos/Private-test-bank/senior-dev-windows/-/commits/master) | [![pipeline status](https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-windows/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-windows/-/commits/master) | 

### Misc Pipelines

| Repo | Pipeline Status |
| ---           | ------ |
| Combined Template | [![pipeline status](https://gitlab.com/90cos/public/training/templates/combined-template/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/templates/combined-template/-/commits/master) |
| MDBook Template | [![pipeline status](https://gitlab.com/90cos/public/training/templates/mdbook-template/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/templates/mdbook-template/-/commits/master) |
| Slides Template | [![pipeline status](https://gitlab.com/90cos/public/training/templates/slides-template/badges/master/pipeline.svg)](https://gitlab.com/90cos/public/training/templates/slides-template/-/commits/master) |


## To contribute to the MTTL:
1.  Review the schema to ensure you have the neccesary fields for your contribution, please see the folowing link: https://gitlab.com/90COS/public/mttl/-/wikis/Process/MTTL-Automation-Design#knowledge-skill-ability-and-task-json-files
2.  a) Click the "Issues" button on the left side bar.

	b) Create a new issue by clicking the "New Issue" button.
![Issue](https://gitlab.com/api/v4/projects/18198350/repository/files/MTTL%2FIssue%2Epng/raw?ref=master)
3.  Select the template that best matches the changes that you are proposing from the "Description" dropdown.

    a) To propose a new workrole, select the "New_Work_Role" template.
    
    b) To propose general changes, select the "General_Contribution" template.
4.  a) Provide a useful title that we can track.

	b) Replace the text wrapped in parentheses with the required information.
	
	c) Provide a Label in the "Labels" dropdown.

Notes: This is intended to gather the contributing information and not how to implement the contributors changes into the