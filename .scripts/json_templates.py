def ksat_list_template(work_roles: list):
    return {
        "title": "Schema for a MTTL requirement JSON file.",
        "definitions": {
            "trn_eval": ksat_trn_eval_template(),
            "wr_spec": ksat_wr_spec_template(work_roles)
        },
        "type": "array",
        "items": ksat_item_template(work_roles),
        "additionalProperties": False
    }

def ksat_item_template(work_roles: list):
    return {
        "title": "Schema for a MTTL requirement JSON file.",
        "definitions": {
            "trn_eval": ksat_trn_eval_template(),
            "wr_spec": ksat_wr_spec_template(work_roles)
        },
        "type": "object",
        "properties": {
            "_id": {"type": "string", "pattern": "^[KSAT][0-9]{4}$"},
            "description": {"type": "string", "minLength": 1},
            "parent": {
                "type": "array",
                "items": {"type": "string", "minLength": 1},
                "minItems": 0
            },
            "topic": {"type": "string", "minLength": 0},
            "ksat_type": {"type": "string", "enum": ['knowledge', 'skills', 'abilities', 'tasks']},
            "requirement_src": {
                "type": "array",
                "items": {"type": "string", "minLength": 1},
                "minItems": 0
            },
            "requirement_owner": {"type": "string", "minLength": 0},
            "comments": {"type": "string", "minLength": 0},
            "training": {"$ref": "#/definitions/trn_eval"},
            "eval": {"$ref": "#/definitions/trn_eval"},
            "work-roles": {"$ref": "#/definitions/wr_spec"},
            "specializations": {"$ref": "#/definitions/wr_spec"}
        },
        "required": [
            "_id",
            "description",
            "parent",
            "topic",
            "requirement_src",
            "requirement_owner",
            "comments",
            "training",
            "eval",
            "work-roles",
            "specializations"
        ]
    }

def ksat_trn_eval_template():
    return {
        "type": "array",
        "items": {
            "type": "object",
            "properties": {
                "$oid": {"type": "string"}
            },
            "required": ["$oid"]
        },
        "minItems": 0
    }

def ksat_wr_spec_template(work_roles: list):
    return {
        "type": "array",
        "items": {
            "type": "object",
            "properties": {
                "_id": {"type": "string", "enum": work_roles},
                "proficiency": {"type": "string", "pattern": "^([A-Da-d1-4]|([1-4][a-d]))$"}
            },
            "required": ["_id"]
        }
    }

def rel_link_list_template(work_roles, rel_link_item_template):
    return {
        "title": "Schema for a rel-link JSON list.",
        "type": "array",
        "items": rel_link_item_template(work_roles),
        "additionalProperties": False
    }

def trn_rel_link_item_template(work_roles):
    return {
        "type": "object",
        "properties": {
            "course": {"type": "string"},
            "module":  {"type": "string"},
            "topic":  {"type": "string"},
            "subject":  {"type": "string"},
            "network":  {"type": "string"},
            "map_for": {"type": "string", "enum": ["eval", "training"]},
            "KSATs": rel_link_mapping_list_template(),
            "lecture_time":  {"type": "integer"},
            "perf_demo_time":  {"type": "integer"},
            "references": {
                "type": "array",
                "items": {"type": "string"}
            },
            # TODO: update to real schema
            "lesson_objectives": {
                "type": "array"
            },
            # TODO: update to real schema
            "performance_objectives": {
                "type": "array"
            },
            "work-roles": {
                "type": "array",
                "item": {"type": "string", "enum": work_roles}
            }
        },
        "required": [
            "course",
            "module",
            "topic",
            "subject",
            "KSATs",
            "network",
            "map_for",
            "work-roles",
        ]
    }

# old to new eval rel link conversion chart
# NETWORK -> network
# uid -> question_id
# version -> language
# proficiency -> question_proficiency
# etc -> estimated_time_to_complete
# workroles -> work-roles
def evl_rel_link_item_template(work_roles):
    return {
        "type": "object",
        "properties": {
            "test_id": {"type": "string"},
            "test_type": {"type": "string", "enum": ["knowledge", "performance"]},
            "question_id": {"type": "string"},
            "question_name": {"type": "string"},
            "topic":  {"type": "string"},
            "network":  {"type": "string"},
            "language":  {"type": "string"},
            "map_for": {"type": "string", "enum": ["eval", "training"]},
            "KSATs": rel_link_mapping_list_template(),
            "question_proficiency": {"type": "string", "pattern": "^([A-Da-d1-4]|([1-4][a-d]))$"},
            "complexity": {"type": "integer"},
            "estimated_time_to_complete": {"type": "number"},
            "disabled": {"type": "boolean"},
            "provisioned": {"type": "integer"},
            "attempts": {"type": "integer"},
            "passes": {"type": "integer"},
            "failures": {"type": "integer"},
            "work-roles": {
                "type": "array",
                "item": {"type": "string", "enum": work_roles}
            }
        },
        "required": [
            "test_id",
            "test_type",
            "question_id",
            "question_name",
            "topic",
            "network",
            "language",
            "question_proficiency",
            "complexity",
            "estimated_time_to_complete",
            "work-roles",
            "KSATs",
            "map_for"
        ]
    }

def rel_link_mapping_list_template():
    return {
        "type": "array",
        "item": rel_link_mapping_item_template()
    }
def rel_link_mapping_item_template():
    return {
        "type": "object",
        "properties": {
            "ksat_id": {"type": "string", "pattern": "^[KSAT][0-9]{4}$"},
            "item_proficiency": {"type": "string", "pattern": "^([A-Da-d1-4]|([1-4][a-d]))$"},
            "url": {"type": "string"}
        },
        "required": [
            "ksat_id",
            "item_proficiency",
            "url"
        ]
    }