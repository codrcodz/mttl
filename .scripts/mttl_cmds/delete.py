#!/usr/bin/python3

import os
import re
import pymongo
import argparse
from datetime import date
from bson.objectid import ObjectId

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])


def ksat_regex_type_validation(arg_value, pat=re.compile(r"^[KSAT][0-9]{4}$")):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value
def rel_link_regex_type_validation(arg_value, pat=re.compile(r"^[0-9a-f]{24}$")):
    if not pat.match(arg_value):
        raise argparse.ArgumentTypeError
    return arg_value

def delete_ksat_data(reqs: object, rls: object, ksat_id: str):
    ret = reqs.delete_one({'_id': ksat_id})
    if ret.deleted_count < 1:
        print(f'No KSAT associated with {ksat_id}')
        exit(1)

    # remove ksat_id from all rel_links that maps to it
    rls.update_many(
        {'KSATs.ksat_id': ksat_id},
        {
            '$pull': {'KSATs': {'ksat_id': ksat_id}},
            '$set': { 'updated_on': f'{date.today()}' }
        }
    )

def delete_rel_link_data(reqs: object, rls: object, id: str):
    rel_link = rls.find_one({'_id': ObjectId(id)})
    if rel_link != None:
        # remove rel-link id on all KSATs that have it mapped
        reqs.update_many(
            { '_id': {'$in': [_id['ksat_id'] for _id in rel_link['KSATs']]}},
            { 
                '$pull': { rel_link['map_for']: rel_link['_id'] },
                '$set': { 'updated_on': f'{date.today()}' }
            }
        )
        # remove the rel-link
        rls.delete_one({'_id': ObjectId(id)})
    else:
        print(f'No rel-link associated with {id}')

def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    parser = argparse.ArgumentParser(
                        description=f'delete script to remove ksat/rel-link objects from datasets')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--ksat', nargs='+', type=ksat_regex_type_validation,
                        metavar="KSAT_ID", help="specify one or more KSAT_IDs to delete")
    group.add_argument('--rel-link', nargs='+', type=rel_link_regex_type_validation,
                        metavar="REL_LINK_ID", help="specify one or more REL_LINK_IDs to delete")
    parsed_args = parser.parse_args()

    if parsed_args.ksat:
        for item in parsed_args.ksat:
            delete_ksat_data(reqs, rls, item)
    elif parsed_args.rel_link:
        for item in parsed_args.rel_link:
            delete_rel_link_data(reqs, rls, item)


if __name__ == "__main__":
    main()