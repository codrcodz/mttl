#!/usr/bin/python3

import os
import argparse
import unittest
import subprocess
import pymongo
import json
import random
from find import find_query
from bson.objectid import ObjectId
from insert import insert_ksat_data, insert_rel_link_data, get_new_ksat_key

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])

client = pymongo.MongoClient(HOST, PORT)
db = client.mttl
reqs = db.requirements
rls = db.rel_links


class TestMethods(unittest.TestCase):
    def test_insert_ksat(self):
        # make sure it does not exist already
        self.assertEqual(len(find_query(reqs, 'ksat', {'_id': get_new_ksat_key(reqs, 'knowledge')}, None)), 0)
        test_data = {
            'ksat_type': 'knowledge',
            'description': 'This is a test description',
            'topic': 'test',
            'parent': ['K0001'],
            'requirement_src': [],
            'requirement_owner': "",
            'comments': 'test comments',
            'training': [],
            'eval': [],
            'work-roles': [],
            'specializations': []
        }
        new_id = insert_ksat_data(reqs, test_data)
        found_items = find_query(reqs, 'ksat', {'_id': new_id}, None, False)
        # make sure it now exist
        self.assertEqual(len(found_items), 1)
        self.assertEqual(found_items[0]['_id'], new_id)
        self.assertEqual(found_items[0]['description'], 'This is a test description')
        self.assertEqual(found_items[0]['topic'], 'test')
        self.assertEqual(found_items[0]['parent'][0], 'K0001')
        self.assertEqual(found_items[0]['comments'], 'test comments')

    def test_insert_trn_rel_link(self):
        # make sure it does not exist already
        self.assertEqual(len(find_query(rls, 'rel-link', {'$and': [{'course': 'test_course'}, {'module': 'my_module'}]}, None, False)), 0)
        test_data = {
            'course': 'test_course',
            'module': 'my_module',
            'topic': 'test_topic',
            'subject': 'test_subject',
            'KSATs': [
                {
                    'ksat_id': 'K0001',
                    'item_proficiency': 'D',
                    'url': 'https://my.test.url'
                }
            ],
            'network': 'test_network',
            'map_for': 'training',
            'work-roles': ['Basic-Dev']
        }
        new_id = insert_rel_link_data(reqs, rls, test_data)
        found_items = find_query(rls, 'rel-link', {'$and': [{'course': 'test_course'}, {'module': 'my_module'}]}, None, False)
        # make sure it now exist
        self.assertEqual(len(found_items), 1)
        self.assertEqual(found_items[0]['_id'], new_id)
        self.assertEqual(found_items[0]['course'], 'test_course')
        self.assertEqual(found_items[0]['module'], 'my_module')
        self.assertEqual(found_items[0]['topic'], 'test_topic')
        self.assertEqual(found_items[0]['subject'], 'test_subject')
        for i, item in enumerate(test_data['KSATs']):
            self.assertEqual(found_items[0]['KSATs'][i]['ksat_id'], item['ksat_id'])
            self.assertEqual(found_items[0]['KSATs'][i]['item_proficiency'], item['item_proficiency'])
            self.assertEqual(found_items[0]['KSATs'][i]['url'], item['url'])
        self.assertEqual(found_items[0]['network'], 'test_network')
        self.assertEqual(found_items[0]['work-roles'][0], 'Basic-Dev')

        ksat = find_query(reqs, 'ksat', {'_id': 'K0001'}, None, False)
        link_made = False
        for link in ksat[0]['training']:
            if str(link) == str(new_id):
                link_made = True
        self.assertTrue(link_made)

    def test_insert_evl_rel_link(self):
        # make sure it does not exist already
        self.assertEqual(len(find_query(rls, 'rel-link', {'$and': [{'question_id': 'test_question'}, {'test_id': 'test_id'}]}, None, False)), 0)
        test_data = {
            'test_id': 'test_id',
            'test_type': 'knowledge',
            'question_id': 'test_question',
            'topic': 'test_topic',
            'language': 'test_lang',
            'question_proficiency': 'D',
            'complexity': 0,
            'estimated_time_to_complete': 0,
            'KSATs': [
                {
                    'ksat_id': 'K0001',
                    'item_proficiency': 'D',
                    'url': 'https://my.test.url'
                }
            ],
            'network': 'test_network',
            'map_for': 'eval',
            'work-roles': ['Basic-Dev']
        }
        new_id = insert_rel_link_data(reqs, rls, test_data)
        found_items = find_query(rls, 'rel-link', {'$and': [{'question_id': 'test_question'}, {'test_id': 'test_id'}]}, None, False)
        # make sure it now exist
        self.assertEqual(len(found_items), 1)
        self.assertEqual(found_items[0]['_id'], new_id)
        self.assertEqual(found_items[0]['test_id'], 'test_id')
        self.assertEqual(found_items[0]['test_type'], 'knowledge')
        self.assertEqual(found_items[0]['question_id'], 'test_question')
        self.assertEqual(found_items[0]['language'], 'test_lang')
        self.assertEqual(found_items[0]['question_proficiency'], 'D')
        self.assertEqual(found_items[0]['complexity'], 0)
        self.assertEqual(found_items[0]['estimated_time_to_complete'], 0)
        for i, item in enumerate(test_data['KSATs']):
            self.assertEqual(found_items[0]['KSATs'][i]['ksat_id'], item['ksat_id'])
            self.assertEqual(found_items[0]['KSATs'][i]['item_proficiency'], item['item_proficiency'])
            self.assertEqual(found_items[0]['KSATs'][i]['url'], item['url'])
        self.assertEqual(found_items[0]['network'], 'test_network')
        self.assertEqual(found_items[0]['work-roles'][0], 'Basic-Dev')


if __name__ == "__main__":
    unittest.main()