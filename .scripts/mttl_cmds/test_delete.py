#!/usr/bin/python3

import os
import argparse
import unittest
import subprocess
import pymongo
import json
import random
from find import find_query

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])


class TestMethods(unittest.TestCase):
    def test_delete(self):
        self.assertTrue(True)

if __name__ == "__main__":
    unittest.main()