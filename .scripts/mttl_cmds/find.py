#!/usr/bin/python3

import os
import re
import sys
import pymongo
import argparse
import json
from pprint import pprint
from datetime import date
from bson.objectid import ObjectId

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])


def find_query(collection: object, type_str:str, find_query:dict, projection:dict, print_val=True):
    ret_docs = None
    projection_query = {}
    if type_str == 'rel-link':
        if '_id' in find_query:
            find_query['_id'] = ObjectId(find_query['_id'])
    if projection != None:
        for item in projection:
            projection_query.update({item: 1})
        ret_docs = collection.find(find_query, projection_query)
    else:
        ret_docs = collection.find(find_query)

    ret = []
    for item in ret_docs:
        ret.append(item)
        if print_val:
            print()
            pprint(item)
            print()
    return ret



def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    parser = argparse.ArgumentParser(
                        description=f'find script to print MTTL data to stdout')
    type_group = parser.add_mutually_exclusive_group(required=True)    
    type_group.add_argument('--ksat', action='store_true',
                        help="updating KSATs")
    type_group.add_argument('--rel-link', action='store_true',
                        help="updating rel-links")
    parser.add_argument('-p', '--projection', nargs='+', type=str,
                        default=None, help='only return these fields to stdout')
    parser.add_argument('find_query', type=json.loads,
                        help="mongodb query to find document to output '{...}' (wrap KEYs and VALs in double quotes)")
    parsed_args = parser.parse_args()
    if parsed_args.ksat:
        ret = find_query(reqs, 'ksat', parsed_args.find_query, parsed_args.projection)
    elif parsed_args.rel_link:
        ret = find_query(rls, 'rel-link', parsed_args.find_query, parsed_args.projection)
    
    return ret

if __name__ == "__main__":
    main()