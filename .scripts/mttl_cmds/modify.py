#!/usr/bin/python3

import os
import re
import sys
import pymongo
import argparse
import jsonschema
import json
from datetime import date
from bson.objectid import ObjectId
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from json_templates import rel_link_mapping_item_template

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])

ksat_avoid_field = ['training', 'eval']

def validate_insert_mapping_data(data: dict, schema_func):
    '''
    validate all insertion data
    '''
    try: 
        jsonschema.validate(instance=data, schema=schema_func())
    except jsonschema.exceptions.ValidationError as err:
        print(err)
        exit(1)

def update_query(collection: object, find_query: dict, update_query: dict):
    found = list(collection.find(find_query, {"_id": 1, "map_for": 1}))
    update_list = ', '.join([str(item['_id']) for item in found])
    print(f'Will be Updated: {update_list}')
    ret = collection.update_many(find_query, update_query)

    if ret.modified_count == 0:
        print('No KSATs were modified')
        exit(1)

def insert_rel_link_mapping(reqs, rls, find_query: dict, mapping: list):
    new_map = {
        'ksat_id': mapping[0],
        'item_proficiency': mapping[1],
        'url': mapping[2]
    } 
    validate_insert_mapping_data(new_map, rel_link_mapping_item_template)

    if '_id' in find_query:
        find_query['_id'] = ObjectId(find_query['_id'])

    found = list(rls.find(find_query, {"_id": 1, "map_for": 1}))
    update_list = ', '.join([str(item['_id']) for item in found])
    print(f'Insert KSAT mappings into: {update_list}')
    # will insert KSATs into all rel-links found with the find-query 
    rls.update_many(find_query,
        { 
            '$push': { 
                "KSATs": new_map
            }
        }
    )

    # itterate through all of the rel-link object _ids
    for item in found:
        # update all KSAT passed in to delete rel-link _id
        reqs.update_one(
            { '_id': mapping[0] },
            { '$push': { 
                item['map_for']: item['_id']
            }}
        )

def update_rel_link_mapping(rls, find_query, mapping):
    mapping = mapping.split(':')

    if '_id' in find_query:
        find_query['_id'] = ObjectId(find_query['_id'])
    find_query.update({ 'KSATs.ksat_id': mapping[0] })

    update_list = ', '.join([str(item['_id']) for item in list(rls.find(find_query, {"_id": 1}))])
    print(f'Update KSAT mapping in: {update_list}')

    ret = rls.update_many(find_query,
        { '$set' : { 'KSATs.$.module_proficiency': mapping[1], 'updated_on': f'{date.today()}' }}
    )

def delete_rel_link_mapping(reqs, rls, find_query: dict, ksat: list):
    if '_id' in find_query:
        find_query['_id'] = ObjectId(find_query['_id'])
    
    found = list(rls.find(find_query, {"_id": 1, "map_for": 1}))
    update_list = ', '.join([str(item['_id']) for item in found])
    print(f'Delete KSAT mappings from: {update_list}')
    if len(update_list) == 0:
        print('Did not find any matching rel-links from the find-query')
        exit(1)

    # will delete KSATs from all rel-links within find-query 
    rls.update_many(find_query,
        { 
            '$pull': { 
                "KSATs": { 
                    'ksat_id': { '$in': ksat } 
                } 
            }
        }
    )

    # itterate through all of the rel-link object _ids
    for item in found:
        # update all KSAT passed in to delete rel-link _id
        reqs.update_many(
            { '_id': { '$in': ksat } },
            { '$pull': { 
                item['map_for']: item['_id']
            }}
        )


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    rls = db.rel_links

    parser = argparse.ArgumentParser(
                        description=f'modify script to update ksat/rel-link objects in datasets')
    type_group = parser.add_mutually_exclusive_group(required=True)    
    type_group.add_argument('--ksat', action='store_true',
                        help="updating KSATs")
    type_group.add_argument('--rel-link', action='store_true',
                        help="updating rel-links")

    update_group = parser.add_mutually_exclusive_group(required=True)
    update_group.add_argument('-u', '--update-query', type=json.loads,
                        help="mongodb query to update specified documents found by find-query '{...}' (wrap KEYs and VALs in double quotes)")
    update_group.add_argument('-i', '--insert-mapping', nargs=3, type=str, metavar=('KSAT', 'PROFICIENCY', 'URL'),
                        help="takes three values ksat_id, item_proficiency, and url for the mapping")
    update_group.add_argument('-p', '--modify-mapping-proficiency', type=str, nargs='+',
                        help="list (space delimited) of KEY:VAL pairs of KSAT:NEW_PROFICIENCY to update mappings of documents found by find-query")
    update_group.add_argument('-d', '--delete-mapping', type=str, nargs='+',
                        help="list (space delimited) of KSATs to delete KSAT mappings on documents found by find-query")

    parser.add_argument('-f', '--find-query', type=json.loads, required=True,
                        help="mongodb query to find document to update '{...}' (wrap KEYs and VALs in double quotes)")
    parsed_args = parser.parse_args()


    if parsed_args.ksat:
        if parsed_args.update_query:
            update_query(reqs, parsed_args.find_query, parsed_args.update_query)
    elif parsed_args.rel_link:
        if parsed_args.update_query:
            update_query(rls, parsed_args.find_query, parsed_args.update_query)
        elif parsed_args.insert_mapping:
            insert_rel_link_mapping(reqs, rls, parsed_args.find_query, parsed_args.insert_mapping)
        elif parsed_args.modify_mapping_proficiency:
            # itterate every space delimited string
            for mapping in parsed_args.modify_mapping_proficiency:
                update_rel_link_mapping(rls, parsed_args.find_query, mapping)
        elif parsed_args.delete_mapping:
            # itterate every space delimited string
            delete_rel_link_mapping(reqs, rls, parsed_args.find_query, parsed_args.delete_mapping)


if __name__ == "__main__":
    main()