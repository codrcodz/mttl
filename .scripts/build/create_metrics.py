#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
import mongo_helpers

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])

output_root = '.frontend/src/data'

def update_metrics(metrics:dict, key:str, with_both:int, with_training:int, with_eval:int, total:int, without_training_set:list, without_eval_set:list):
    metrics.update({ key: { 
        'percent-total': (with_both / total) * 100 if total > 0 else 0,
        'percent-training': (with_training / total) * 100 if total > 0 else 0,
        'percent-eval': (with_eval / total) * 100 if total > 0 else 0,
        'ksas-without-training': without_training_set,
        'ksas-without-eval': without_eval_set
    }})

def create_mttl_metrics(collection:object, metrics:dict):
    '''
    create mttl metrics in the 'metrics' dict
    '''
    # query mongodb for various information that we convert into a list of strings
    without_training_set = [ksat['_id'] for ksat in collection.find({'training': {'$size': 0}}, sort=[('_id', 1)])]
    without_eval_set = [ksat['_id'] for ksat in collection.find({'eval': {'$size': 0}}, sort=[('_id', 1)])]

    # count all ksats with training and eval
    with_training = collection.count_documents({'training': {'$not': {'$size': 0}}})
    with_eval = collection.count_documents({'eval': {'$not': {'$size': 0}}})
    with_both = collection.count_documents({
            'training': {'$not': {'$size': 0}},
            'eval': {'$not': {'$size': 0}}
        })
    # count all ksats for total
    total = collection.count_documents({})

    update_metrics(metrics, 'MTTL', with_both, with_training, with_eval, total, without_training_set, without_eval_set)

def append_ttl_metrics(collection:object, metrics:dict, work_role_key:str, wrspec:str):
    '''
    create ttl metrics in the 'metrics' dict
    '''
    if work_role_key not in metrics:
        metrics[work_role_key] = {}

    # query mongodb for various information that we convert into a list of strings
    without_training_set = [ksat['_id'] for ksat in collection.find({
        f'{work_role_key}._id': wrspec,
        'training': {'$size': 0}
    }, sort=[('_id', 1)])]
    without_eval_set = [ksat['_id'] for ksat in collection.find({
        f'{work_role_key}._id': wrspec,
        'eval': {'$size': 0}
    }, sort=[('_id', 1)])]

    # count all ksats with training
    with_training = collection.count_documents({
            f'{work_role_key}._id': wrspec,
            'training': {'$not': {'$size': 0}}
        })
    # count all ksats with eval
    with_eval = collection.count_documents({
            f'{work_role_key}._id': wrspec,
            'eval': {'$not': {'$size': 0}}
        })
    # count all ksats with eval and training
    with_both = collection.count_documents({
            f'{work_role_key}._id': wrspec,
            'training': {'$not': {'$size': 0}},
            'eval': {'$not': {'$size': 0}}
        })
    # count all ksats for total
    total = collection.count_documents({
        f'{work_role_key}._id': wrspec
    })

    update_metrics(metrics[work_role_key], wrspec, with_both, with_training, with_eval, total, without_training_set, without_eval_set)

def create_ttl_metrics(collection:object, metrics:dict, work_roles:list, specializations:list):
    '''
    Starter function for ttl metrics to pass individual work-roles/specializations into append_ttl_metrics
    '''
    for work_role in work_roles:
        append_ttl_metrics(collection, metrics, 'work-roles', work_role)
    for specialization in specializations:
        append_ttl_metrics(collection, metrics, 'specializations', specialization)
    

def create_nowrspec_metrics(collection:object, metrics:dict):
    without_wrspec = collection.count_documents({'work-roles': {'$size': 0},'specializations': {'$size': 0}})
    total = collection.count_documents({})

    metrics.update({ 'nowrspec': {
        'percent-total': (without_wrspec / total) * 100 if total > 0 else 0
    }})

def main():
    global output_root

    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements
    
    metrics = {}
    work_roles = []
    output_path = os.path.join(output_root, 'metrics.min.json')
  
    # for work_role_path in work_role_paths:
    with open('WORK-ROLES.json', 'r') as wr_spec_file:
        work_roles = list(json.load(wr_spec_file).keys())
    with open('SPECIALIZATIONS.json', 'r') as wr_spec_file:
        specializations = list(json.load(wr_spec_file).keys())

    create_mttl_metrics(reqs, metrics)
    create_ttl_metrics(reqs, metrics, work_roles, specializations)
    create_nowrspec_metrics(reqs, metrics)

    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w') as metrics_file:
        json.dump(metrics, metrics_file, sort_keys=False, separators=(',', ':'))

if __name__ == "__main__":
    main()