#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json
import mongo_helpers

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])


output_root = '.frontend/src/data'
log_output_path = os.path.join(output_root, 'create_wrspec_dataset.log') 

filename = os.path.splitext(os.path.basename(__file__))[0]

def key_picker(ksa: str):
    return {
        'T': 'Tasks',
        'A': 'Abilities',
        'S': 'Skills',
        'K': 'Knowledge'
    }[ksa[0]]

def query_and_itterate_wrspec(collection:object, wrspec_key:str, wrspec:str, outdata:dict):
    outdata[wrspec] = { 
        'wr_spec': wrspec,
        'Tasks': '',
        'Abilities': '',
        'Skills': '',
        'Knowledge': ''
    }

    for ksat in collection.find({f'{wrspec_key}._id': wrspec}):
        ksat_id = ksat['_id']
        ksat_key = key_picker(ksat_id)
        if len(outdata[wrspec][ksat_key]) == 0:
            outdata[wrspec][ksat_key] = f'{ksat_id}'
        else:
            outdata[wrspec][ksat_key] += f', {ksat_id}'

def create_wrspec_dataset(collection:object, work_roles:list, specializations:list):
    global output_root
    wrspec_data = {}
    wrspec_output_path = os.path.join(output_root, 'wr_spec.min.json')

    # itterate through all work-role names
    for work_role in work_roles:
        query_and_itterate_wrspec(collection, 'work-roles', work_role, wrspec_data)
    # itterate through all specialization names
    for specialization in specializations:
        query_and_itterate_wrspec(collection, 'specializations', specialization, wrspec_data)

    os.makedirs(output_root, exist_ok=True)
    with open(wrspec_output_path, 'w+') as fd:
        json.dump(list(wrspec_data.values()), fd, sort_keys=False, separators=(',', ':'))

def create_nowrspec_dataset(collection:object):
    global output_root
    nowrspec_output_path = os.path.join(output_root, 'nowr_spec.min.json')

    wrdata = {
        'Tasks': [],
        'Abilities': [],
        'Skills': [],
        'Knowledge': []
    }

    for ksat in collection.find({'work-roles': { '$size': 0 },'specializations': { '$size': 0 }}):
        ksat_id = ksat['_id']
        wrdata[key_picker(ksat_id)].append(ksat_id)

    for key in wrdata.keys():
        wrdata[key] = ', '.join(wrdata[key])

    os.makedirs(output_root, exist_ok=True)
    with open(nowrspec_output_path, 'w+') as fd:
        json.dump([wrdata], fd, sort_keys=False, separators=(',', ':'))

def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    global error, output_root, log_output_path
    work_roles = []  
    specializations = []

    with open('WORK-ROLES.json', 'r') as wr_spec_file:
        work_roles = list(json.load(wr_spec_file).keys())
    with open('SPECIALIZATIONS.json', 'r') as wr_spec_file:
        specializations = list(json.load(wr_spec_file).keys())

    create_wrspec_dataset(reqs, work_roles, specializations)
    create_nowrspec_dataset(reqs)

if __name__ == "__main__":
    main()