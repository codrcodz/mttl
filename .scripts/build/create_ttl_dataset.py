#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from helpers import get_all_json, convert_lists
import mongo_helpers

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])

ttldata = {}

def query_and_change_ksat(collection: object, key: str, wrspec: str):
    global ttldata
    # find and itterate through wrspec ksats
    ttldata[wrspec] = list(collection.find({f'{key}._id': wrspec}, sort=[('_id', 1)]))
    for ksat in ttldata[wrspec]:
        for tmpwr in ksat[key]:
            if tmpwr['_id'] == wrspec and 'proficiency' in tmpwr:
                ksat['proficiency'] = tmpwr['proficiency']
        convert_lists(ksat)
        ksat['children'] = '' if 'children' not in ksat else ksat['children']


def main():
    global ttldata
    output_root = '.frontend/src/data'
    output_path = os.path.join(output_root, 'TTLs.min.json')

    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    with open('WORK-ROLES.json', 'r') as wr_spec_file:
        work_roles = list(json.load(wr_spec_file).keys())
    with open('SPECIALIZATIONS.json', 'r') as wr_spec_file:
        specializations = list(json.load(wr_spec_file).keys())

    # iterate through all work-roles
    for key in work_roles:
        # query for work-roles ksats and itterate
        query_and_change_ksat(reqs, 'work-roles', key)
    # iterate through all specializations
    for key in specializations:
        # query for specializations ksats and itterate
        query_and_change_ksat(reqs, 'specializations', key)

    os.makedirs(output_root, exist_ok=True)
    with open(output_path, 'w') as ttfile:
        json.dump(ttldata, ttfile, sort_keys=False, separators=(',', ':'))

if __name__ == "__main__":
    main()