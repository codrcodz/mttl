#!/usr/bin/python3

import os
import sys
import json
import pymongo
sys.path.insert(1, os.path.join(sys.path[0], '..'))
from mongo_helpers import find_array_not_empty, find_id_and_append_array

HOST = 'localhost'
PORT = 27017
# if in pipeline, override HOST
if 'MONGO_HOST' in os.environ:
    HOST = os.environ['MONGO_HOST']
# if in pipeline, override PORT
if 'MONGO_PORT' in os.environ:
    PORT = int(os.environ['MONGO_PORT'])


def main():
    client = pymongo.MongoClient(HOST, PORT)
    db = client.mttl
    reqs = db.requirements

    # itterate all KSATs with parents
    for item in find_array_not_empty(reqs, 'parent'):
        for parent in item['parent']:
            find_id_and_append_array(reqs, parent, 'children', item['_id'], upsert=False)  

if __name__ == "__main__":
    main()