#!/bin/bash
whoami
declare -a evalrepos=(
    "https://gitlab.com/90cos/public/evaluations/preparatory/basic-dev.git"
    "https://gitlab.com/90cos/public/evaluations/preparatory/basic-po.git"
    "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-linux.git"
    "https://gitlab.com/90cos/public/evaluations/preparatory/senior-dev-windows.git"
    )
declare -a trainrepos=(
    "https://gitlab.com/90cos/public/training/lessons/python.git"
    "https://gitlab.com/90cos/public/training/lessons/c-programming.git"
    "https://gitlab.com/90cos/public/training/lessons/reverse-engineering.git"
    "https://gitlab.com/90cos/public/training/lessons/algorithms.git"
    "https://gitlab.com/90cos/public/training/lessons/powershell.git"
    "https://gitlab.com/90cos/public/training/lessons/debugging.git"
    "https://gitlab.com/90cos/public/training/lessons/pseudocode.git"
    "https://gitlab.com/90cos/public/training/lessons/cpp-programming.git"
    "https://gitlab.com/90cos/public/training/lessons/introduction-to-git.git"
    "https://gitlab.com/90cos/public/training/lessons/assembly.git"
    "https://gitlab.com/90cos/public/training/lessons/network-programming.git"
    "https://gitlab.com/90cos/public/training/courses/external/scrum.org-professional-scrum-product-owner-i.git"
    "https://gitlab.com/90cos/public/training/courses/external/actp-linux.git"
    "https://gitlab.com/90cos/public/training/material/see-basics.git"
)
mkdir eval-repos
cd eval-repos
for i in "${evalrepos[@]}"
do
    git clone "$i"
done
cd ..
mkdir training-repos
cd training-repos
for i in "${trainrepos[@]}"
do
    git clone "$i"
done
