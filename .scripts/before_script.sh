#!/bin/bash

if [ -z ${MONGO_HOST} ]; then
    MONGO_HOST=localhost;
fi

# import all requirement files into mongodb requirements colleciton
if [ -d "requirements" ]; then
    for f in requirements/*.json
    do
        until mongoimport --host $MONGO_HOST --db mttl --collection requirements --file $f --jsonArray; do sleep 1; done
    done
fi

# import all rel-link files into mongodb rel_links collection
if [ -d "rel-links/training" ]; then
    for f in rel-links/training/*.rel-links.json
    do
        mongoimport --host $MONGO_HOST --db mttl --collection rel_links --file $f --jsonArray
    done
fi
if [ -d "rel-links/eval" ]; then
    for f in rel-links/eval/*.rel-links.json
    do
        mongoimport --host $MONGO_HOST --db mttl --collection rel_links --file $f --jsonArray
    done
fi
